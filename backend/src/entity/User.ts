import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";
import { Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsOptional } from "class-validator";

@Entity()
export class User {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Length(2, 64)
  firstName: string;

  @Column()
  @Length(2, 64)
  lastName: string;

  @Column({ nullable: true })
  @IsOptional()
  @IsInt()
  @Min(0)
  @Max(120)
  age: number;
}
