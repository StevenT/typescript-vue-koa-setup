import * as Koa from "koa";
import * as Static from "koa-static";
import "reflect-metadata";
import { createConnection } from "typeorm";

import * as json from "koa-json";
import * as logger from "koa-logger";
import * as bodyParser from "koa-bodyparser";

import apiRouter from "./routes";

const PORT = 3000;

createConnection().then(async connection => {
  const app = new Koa();

  /** Middlewares */
  app.use(json());
  app.use(logger());
  app.use(bodyParser());

  /** Static files*/
  app.use(Static("../frontend/dist"));

  /** Routes */
  app.use(apiRouter.routes());
  app.use(apiRouter.allowedMethods());

  /** Start listening */
  app.listen(PORT, () =>
    console.log(`Server started on http://localhost:${PORT}`),
  );

}).catch(error => console.log("TypeORM connection error: ", error));
