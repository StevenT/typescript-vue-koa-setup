import * as Router from "koa-router";
import { getManager } from "typeorm";
import { User } from "../entity/User";
import { PublicUser } from "../../../shared/models";
import mapper, { Source, Schema } from "../services/Mapper";
import { validate } from "class-validator";

const userSchema: Schema<PublicUser> = {
  id: "id",
  firstName: { path: "firstName", default: null },
  lastName: { path: "lastName", default: null },
};

const userMapper = mapper<PublicUser>(userSchema, {
  name: "user mapper",
});

function userToApi(user: User) {
  return userMapper(user);
}

const router = new Router({ prefix: "/users" });

router.get("/", async (ctx, next) => {
  const userRepository = getManager().getRepository(User);
  const users = await userRepository.find();
  ctx.body = users.map(userToApi);
  await next();
});

router.post("/", async (ctx, next) => {
  const userRepository = getManager().getRepository(User);
  const newUser = userRepository.create(ctx.request.body);

  const errors = await validate(newUser);
  if (errors.length > 0) {
    ctx.status = 406;
    ctx.body = errors;
    return;
  }

  await userRepository.save(newUser);
  ctx.status = 204;
});

router.get("/:id", async (ctx, next) => {
  const userRepository = getManager().getRepository(User);
  const user = await userRepository.findOne((ctx as any).params.id);

  if (!user) {
    ctx.status = 404;
    return;
  }

  ctx.body = userToApi(user);
  await next();
});

router.put("/:id", async (ctx, next) => {
  const userRepository = getManager().getRepository(User);
  const user = await userRepository.findOne((ctx as any).params.id);

  if (!user) {
    ctx.status = 404;
    return;
  }

  user.firstName = ctx.request.body.firstName;
  user.lastName = ctx.request.body.lastName;

  const errors = await validate(user);
  if (errors.length > 0) {
    ctx.status = 406;
    ctx.body = errors;
    return;
  }
  await userRepository.save(user);
  ctx.status = 204;
});

router.delete("/:id", async (ctx, next) => {
  const userRepository = getManager().getRepository(User);
  const user = await userRepository.findOne((ctx as any).params.id);

  if (!user) {
    ctx.status = 404;
    return;
  }

  await userRepository.delete(user);
  ctx.status = 204;
});

export default router;
