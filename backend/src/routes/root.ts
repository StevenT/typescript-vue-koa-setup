import * as Router from "koa-router";

const router = new Router({ prefix: "" });

router.get("/", async (ctx, next) => {
  ctx.body = "Hello";
  next();
});

export default router;
