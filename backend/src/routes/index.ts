import * as Router from "koa-router";

import rootRouter from "./root";
import userRouter from "./user";

const apiRouter = new Router({ prefix: "/api" });

const nestedRoutes = [
  rootRouter,
  userRouter,
];
for (const router of nestedRoutes) {
  apiRouter.use(router.routes(), router.allowedMethods());
}

export default apiRouter;
