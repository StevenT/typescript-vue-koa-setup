# Build tasks
.PHONY:build-frontend
build-frontend:
	cd frontend && npm run build

.PHONY:build-backend
build-backend:
	cd backend && npm run build

.PHONY:build-tasks
build-tasks: build-frontend build-backend
	@echo "Task build => Finished"

.PHONY:build
build:
	${MAKE} -j 2 build-tasks

# Development tasks
.PHONY:dev-frontend
dev-frontend:
	cd frontend && npm run serve

.PHONY:dev-backend
dev-backend:
	cd backend && npm run serve

.PHONY:dev-tasks
dev-tasks: dev-frontend dev-backend
	@echo "Task dev => finished"

.PHONY:dev
dev:
	${MAKE} -j 2 dev-tasks

# FIN