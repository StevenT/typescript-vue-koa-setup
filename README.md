# MyBlog

## Setup with Vue and Koa in Typescript

This is a test setup for a Vue 2.x frontend being served with a Koa Node server. The Koa also serves an API.
Everything is written in Typescript and for data interfaces are shared between the backend and frontend.

### development

```sh
make dev
```

### building

```sh
make build
```

### run in production

```sh
cd backend/dist && node index.js
```
