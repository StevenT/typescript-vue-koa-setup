export interface PublicUser {
  id: number;
  firstName?: string;
  lastName?: string;
}
