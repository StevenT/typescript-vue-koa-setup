export interface ValidationError<T> {
  target: T;
  property: string;
  value: any;
  constraints: {
    [type: string]: string;
  }
  children?: ValidationError<any>[];
}