module.exports = {
  devServer: {
    proxy: {
      "/api/": {
        target: "http://localhost:3000/",
      },
    },
  },
  // configureWebpack: {
  //   devtool: "source-map",
  // },
  // css: {
  //   loaderOptions: {
  //     sass: {
  //       data: `
  //         @import "@/styles/_variables.scss";
  //         @import "@/styles/_mixins.scss";
  //       `,
  //     },
  //   },
  // },
};
