// inspired by https://github.com/MichalZalecki/mappet

/*eslint @typescript-eslint/no-explicit-any: "off" */

export type Source = {
  [key: string]: any;
};

type SchemaEntry =
  | string
  | {
      path: string;
      modifier?: (value: any, source: any) => any;
      include?: (value: any, source: any) => boolean;
      default?: string | number | boolean | null;
    };

export type Schema<Target> = { [key in keyof Target]: SchemaEntry };

interface MapperOptions {
  name: string;
}

function identity<Target>(val: Target) {
  return val;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function always(_val: any) {
  return true;
}

class MapperError extends Error {
  source: any;
  constructor(message: string, source: any) {
    super(message);
    this.source = source;
  }
}

export default function mapper<Target>(schema: Schema<Target>, options: MapperOptions) {
  const { name } = options;
  return (source: Source) =>
    Object.keys(schema).reduce((result: Partial<Target>, key: string) => {
      const schemaEntry: SchemaEntry = schema[key as keyof Target];
      const include = typeof schemaEntry === "string" ? always : schemaEntry.include || always;
      const defaultValue = typeof schemaEntry === "string" ? undefined : schemaEntry.default;
      const path = typeof schemaEntry === "string" ? schemaEntry : schemaEntry.path;
      const value = source[path];

      if (!include(value, source)) {
        return result;
      }

      const modifier = typeof schemaEntry === "string" ? identity : schemaEntry.modifier || identity;

      if (value === undefined) {
        if (defaultValue !== undefined) {
          return { ...result, [key]: defaultValue };
        }
        throw new MapperError(`${name}: Property "${path}" not found`, source);
      }

      return { ...result, [key]: modifier(value, source) };
    }, {}) as Target;
}
