import { httpService } from "@/services/HttpService";
import { PublicUser } from "@/../../shared/models";
import mapper, { Source, Schema } from "./Mapper";

const userSchema: Schema<PublicUser> = {
  id: "id",
  firstName: { path: "firstName", default: null },
  lastName: { path: "lastName", default: null },
};

const userMapper = mapper<PublicUser>(userSchema, {
  name: "user mapper",
});

function userFromApi(api: Source) {
  return userMapper(api);
}

export default {
  loadUsers() {
    return httpService
      .get<PublicUser[]>("/api/users")
      .then(response => response.data)
      .then(data => data.map(userFromApi));
  },
  addUser(user: PublicUser) {
    return httpService.post<null>(`/api/users`, user);
  },
  loadUser(id: number) {
    return httpService.get<PublicUser>(`/api/users/${id}`).then(response => userFromApi(response.data));
  },
  saveUser(user: PublicUser) {
    return httpService.put<null>(`/api/users/${user.id}`, user);
  },
  deleteUser(user: PublicUser) {
    return httpService.delete<null>(`/api/users/${user.id}`);
  },
};
