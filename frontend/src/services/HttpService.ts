import axios from "axios";

export const httpService = axios.create({
  baseURL: "/",
  timeout: 10000,
});
